# arch-linux-enablezfsbuild

This is a project that builds the linux kernel patched to enable zfs dkms build for ArchLinux.

It generates makepkg from upstream archlinux packages repository makepkg.

Then it posts the makepkg to AUR repository.

Then it builds the kernel and generate gitlab pages with the repository.

# Installation

To install the kernel install the aur package 'linux-enablezfsbuild'.

To add repo hosted on gitlab pages to your archlinux, add the lines to your `/etc/pacman.conf`:

	printf '%s\n' '' '[kamcuk-arch-linux-enablezfsbuild]' 'Server = https://kamcuk.gitlab.io/arch-linux-enablezfsbuild' 'SigLevel = Never' >> /etc/pacman.conf

And then just `pacman -S linux-enablezfsbuild`.

# License

This repo is solely intended for my personal use.

