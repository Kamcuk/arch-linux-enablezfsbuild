#!/bin/bash
IFS=$'\n' files=($(git ls-tree -r master --name-only))
set -x
sudo systemctl start docker
SSH_PRIVATE_KEY=$(<~/.ssh/id_rsa_aur)
mkdir -p ./work/archlinux-linux
docker run -ti --rm \
	-v "$(pwd):/this" \
	-v "$(pwd)/archlinux-linux:/repo/archlinux-linux" \
	-w '/this' \
	-e "SSH_PRIVATE_KEY=$SSH_PRIVATE_KEY" \
	archlinux/base bash -c '
	set -xeuo pipefail
	mkdir -p /repo
	cp -a "$@" /repo
	cd /repo

	./setup_builder.sh 
	sudo -u builder ./work.sh
	' -- "${files[@]}"

