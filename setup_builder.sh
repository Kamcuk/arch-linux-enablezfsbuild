#!/bin/bash
set -xeuo pipefail

cat > /tmp/id_rsa_aur <<<"${SSH_PRIVATE_KEY:-}"
if [[ ! -s /tmp/id_rsa_aur ]]; then
	echo "SSH_PRIVATE_KEY is empty. You have to pass private key to connect to aur.archlinux.org" >&2
	exit 1
fi

printf "%s\n" "" "" "y" "y" |
LC_ALL=C pacman -Suy --noconfirm --needed sudo

useradd -m builder
passwd -d builder
chown -R builder:builder .
chmod +w /etc/sudoers
echo "builder ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
chmod -w /etc/sudoers

sudo -u builder bash -c '
	set -x
	mkdir -p ~/.ssh
	echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
'

