#!/bin/bash
set -euo pipefail

pacman() { sudo pacman "$@"; }
export PACKAGER="Kamil Cukrowski via gitlab <kamilcukrowski@gmail.com>"

set -x

echo "Setting up!"
pacman --noconfirm -S --needed git patch openssh

mkdir -p work
pushd work

if [[ ! -e "packages" ]]; then
	git clone \
		--depth 1 \
		--filter=combine:blob:none+tree:0 \
		--no-checkout \
		"git://git.archlinux.org/svntogit/packages.git" packages
	pushd packages
	git checkout master -- linux/trunk
	popd #packages
fi

cp -va packages/linux/trunk/* .
cp -v ../enablezfsbuild.patch .

patch PKGBUILD < ../this_PKGBUILD.patch

if ! ps -p "${SSH_AGENT_PID:-}" > /dev/null && [ -f /.dockerenv ]; then
	echo "Setting up ssh in docker..."
	eval $(ssh-agent -s)
	ssh-add /tmp/id_rsa_aur
	git config --global user.email "kamilcukrowski@gmail.com"
	git config --global user.name "Kamil Cukrowski"
fi

echo "Cloning AUR repo"	
if [[ -e "linux-enablezfsbuild" ]]; then
	rm -rf linux-enablezfsbuild
fi
git clone ssh://aur@aur.archlinux.org/linux-enablezfsbuild.git linux-enablezfsbuild
pushd linux-enablezfsbuild
{
	if [ ! -e "PKGBUILD" ]; then
		empty=true
	fi

	cp -v ../PKGBUILD ../enablezfsbuild.patch ../config ./
	makepkg --printsrcinfo > .SRCINFO

	changed=false
	if [[ "${empty:-}" = "true" ]] || git diff-index --quiet HEAD --; then
		changed=true
		ver=$(set +x; . PKGBUILD; echo "$pkgver"-"$pkgrel")
		git add -A
		git commit -m "Up to $ver"
		git push
	fi
}
popd #linux-enablezfsbuild

if [[ "$changed" = false ]]; then
	echo "NOTHING CHANGED, so not continuing..."
	echo "BUT AS OF NOT DOING IT ANYWAY FOR TESTING"
	# exit 
fi

echo "Setting git archlinux-linux cache..."
mkdir -p ../archlinux-linux
ln -sf ../archlinux-linux archlinux-linux

echo "Building The Linux Kernel..."
ls -lah
pacman --noconfirm -S --needed \
        bc kmod libelf \
        xmlto python-sphinx python-sphinx_rtd_theme graphviz imagemagick \
        git \
        base-devel
makepkg -s --needed --noconfirm --skippgpcheck -f --syncdeps --noprogressbar

echo "Building repository"
pushd ..
{
	IFS=$'\n' pkgs=($(find work -maxdepth 1 -type f -name '*.pkg.tar.*'))
	repo=public/kamcuk-arch-linux-enablezfsbuild
	mkdir -vp "$(dirname "$repo")"
	cp -va "${pkgs[@]}" "$(dirname "$repo")"
	repo-add -n "$repo".db.tar.xz "${pkgs[@]}"
}
popd #..

echo "SUCCESS"

